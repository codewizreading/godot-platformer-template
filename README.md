# Godot Platformer Template

Godot provides several [demo projects](https://github.com/godotengine/godot-demo-projects), including [a 2D platformer](https://github.com/godotengine/godot-demo-projects/tree/master/2d/platformer). This repo contains a version of that demo that's missing a level (and has the volume of the music turned down). The idea is to give this project to students and have them create a level for the game. I use this activity as an introduction to Godot's editor.
